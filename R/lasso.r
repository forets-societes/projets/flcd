#' pixel_fusedlasso
#'
#' @param pixel_ts (numeric vector). Pixel time series
#' @return vector of fitted values
pixel_fusedlasso <- function(pixel_ts) {
  if(any(is.na(pixel_ts))) {
    return(rep(NA, length(pixel_ts)))
  } else {
    res <- genlasso::fusedlasso1d(pixel_ts)
    res.cv <- genlasso::cv.trendfilter(res)
    as.vector(genlasso::softthresh(res, lambda = res.cv$lambda.1se, gamma=0))
  }
}
