#' Remove spatio-temporally isolated impacts
#'
#' @export
#' @param x SpatRaster or vector of character. Single or multilayer raster having dates as first layer
#' @param outFile character. Optional file path of result raster. If missing the cleaned raster will be returned.
#' @param dateWindow integer. Date window width (in days)
#' @param minArea numeric. Minimum area of a patch to be kept (in ha)
#' @param overwrite Logical. If TRUE, "outFile" will be overwritten if it exists.
#' @returns cleaned raster as SpatRaster if `outFile` is missing, otherwise nothing.
removeIsolatedImpacts <- function(x, outFile, dateWindow=15, minArea=0.03, overwrite=FALSE) {

  if(is.character(x)) {
    x <- terra::rast(x)
  }

  if(!inherits(x, "SpatRaster")) {
    stop("Expecting a SpatRaster!", call.=FALSE)
  }

  if(!missing(outFile) && file.exists(outFile) && !overwrite) {
    stop("Out file already exist! Add overwrite=TRUE to replace it!", call.=FALSE)
  }

  # get unique date values
  dates <- sort(terra::unique(x[[1]])[,1])

  # aggregated mask over dates
  mask_dates <- terra::rast(x, nlyrs=1, vals=0)

  # progress <- smartProgressor(length(dates))
  if(isNamespaceLoaded("progressr")) {
    progress <- progressr::progressor(length(dates))
  } else {
    progress <- function(...) {}
  }

  # for each date
  for(date in dates) {

    # extract pixels in date range
    pixels <- terra::classify(x[[1]], t(c(date-dateWindow, date+dateWindow, 1)), others=0)

    # aggregate pixels in patches
    patches <- terra::patches(pixels, directions=8, zeroAsNA=TRUE)

    # compute patch area
    patches_area <- terra::zonal(terra::cellSize(patches, unit="ha"), patches, sum, as.raster=TRUE)

    # keep patches that are big enough and build a mask
    mask_date <- patches_area > minArea
    mask_date <- terra::subst(mask_date, NA, 0)

    # aggregate masks
    mask_dates <- sum(mask_dates, mask_date)

    progress()
  }

  result <- terra::mask(x, mask_dates, maskvalues=0)
  if(missing(outFile)) {
    invisible(result)
  } else {
    terra::writeRaster(result, outFile, overwrite=TRUE)
  }
}
