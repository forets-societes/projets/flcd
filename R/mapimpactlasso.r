#' Detect impacts
#'
#' @param chunk_ts (numeric\[n_pixel, n_date\])
#' @param dates (Date\[n_date\])
#' @param chunk_lasso (numeric\[n_date, n_pixel\])
#' @param dayssum (integer) moving sum window
#' @param threshold (numeric) threshold used to detect an impact
#' @returns a \[n_pixel, 2\] matrix of numerics. First column contains the impact
#' date, the second the magnitude
#'
mapImpactsLasso <- function(chunk_ts, dates, chunk_lasso, dayssum, threshold) {

  # allocate result matrix
  # TODO return only pixels with breakpoints ?
  res <- matrix(NA, nrow = nrow(chunk_ts), ncol=2)

  # compute derivative of lasso output
  deriv1 <- apply(chunk_lasso, 2, diff)
  sum_run <- apply(deriv1, 2, function(l) {
    runner::sum_run(l, k=dayssum, idx=dates[-1], na_rm=TRUE)
  })

  # for each pixel
  for(i in seq_len(nrow(chunk_ts))) {

    sum_runi <- sum_run[, i]
    pixel_tsi <- chunk_ts[i, -1]

    pos <- which(sum_runi<threshold)

    # GCO WARNING magic number 6 !!! (lasso warming)
    # GCO WARNING why only the first sequence ? (discussed in article)
    # GCO WARNING why there is no minimum sequence size ? (accepted)
    if((length(pos)>0) && ( pos[1] > 6 )) {
      firstsequence <- split(pos, cumsum(c(1L, diff(pos) != 1)))[[1]]
      break.pos <- pos[1]
      # date of impact
      res[i, 1] <- dates[break.pos]
      # magnitude of impact
      res[i, 2] <-
        min(pixel_tsi[firstsequence], na.rm=TRUE) -
        stats::median(pixel_tsi[1:(break.pos-1)], na.rm=TRUE)
    }
  }
  res
}
