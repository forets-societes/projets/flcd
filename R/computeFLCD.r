#' Compute FLCD on a raster time series
#'
#' @md
#' @export
#' @param ts SpatRaster or character. Input raster time series as a `SpatRaster` or a vector of raster file paths
#' @param dates Vector of Date. Input dates for each layer of the `ts` raster time series.
#' @param outFile Character. Output raster file path.
#' @param roi Input region of interest. Any object that can be cast to a `SpatExtent`
#' that will be used crop input raster time series `ts`. If `NULL` (default) use extent of `ts` time series.
#' @param perc Numeric. Percentile to use for computing threshold (0<perc<1, default 0.01)
#' @param dayssum Integer. Temporal moving window size (in days, default 90).
#' @param tmpFolder Character. Folder path (created if needed) that will contain intermediate results. Defaults to `terra` tmp folder.
#' @param clean Logical. Should intermediate chunk files be removed on success ? (default TRUE)
#' @param maxPixels Integer. Maximum number of pixels to process at once. Use it if you are hitting memory limits (default to 500 000).
#' @param overwrite Logical. If TRUE, "outFile" will be overwritten if it exists.
#' @returns The resulting raster is saved in `outFile` and contains two layers.
#' | date | date of the detected impact (number of days since 1970-01-01)|
#' |-|-|
#' | magnitude | magnitude of the detected impact |
computeFLCD <- function(ts, dates, outFile, roi=NULL, perc=0.01, dayssum=90,
  tmpFolder=NULL, clean=TRUE, maxPixels=500000, overwrite=FALSE) {

  # sanity check parameters
  # ---------------------------------------------------------------- - - -

  # if time series is given as file names then build raster stack from them
  if(is.character(ts)) {
    if(!all(file.exists(ts))) {
      stop("At least one raster file was not found!", call.=FALSE)
    }
    ts <- terra::rast(ts)
  }

  if(!inherits(ts, "SpatRaster")) {
    stop("Expecting a SpatRaster!", call.=FALSE)
  }

  # TODO check minimum number of layers

  if(terra::nlyr(ts)!=length(dates)) {
    stop("Raster stack and corresponding dates must have same length!", call.=FALSE)
  }

  if(file.exists(outFile) && !overwrite) {
    stop("Out file already exist! Add overwrite=TRUE to replace it!", call.=FALSE)
  }

  if(is.null(tmpFolder)) {
    tmpFolder <- terra::terraOptions(print=FALSE)$tempdir
  }
  dir.create(tmpFolder, showWarnings = FALSE, recursive = TRUE)
  dir.create(file.path(tmpFolder, "chunks"), showWarnings = FALSE, recursive = TRUE)
  dir.create(file.path(tmpFolder, "fused-lasso"), showWarnings = FALSE, recursive = TRUE)
  dir.create(file.path(tmpFolder, "map-impact"), showWarnings = FALSE, recursive = TRUE)

  # prepare data
  # ---------------------------------------------------------------- - - -

  # crop ts to roi
  # TODO should we mask also ?
  if(!is.null(roi)) {
    #ts <- terra::crop(ts, roi)
    terra::window(ts) <- terra::ext(roi)
  }

  # prepare chunking
  # chunk results are cached in separate files
  # if some chunks already exists, check that chunking is the same
  # otherwise stop processing
  chunks <- smartBlocks(ts, maxPixels)
  chunks_file <- file.path(tmpFolder, "chunks.rds")
  if(file.exists(chunks_file)) {
    if(!identical(readRDS(chunks_file), chunks)) {
      warning("Chunking differ from previous one!", immediate. = TRUE, call.=FALSE)
      tmp_chunk_files <- list.files(file.path(tmpFolder, "chunks"), "\\d+.rds")
      if(length(tmp_chunk_files)>0) {
        stop("Remove cached files before!", call. = FALSE)
      }
    }
  } else {
    saveRDS(chunks, chunks_file)
  }

  # Apply FLCD method
  # ---------------------------------------------------------------- - - -

  terra::readStart(ts)
  on.exit(terra::readStop(ts))

  #progress <- smartProgressor(chunks$n)
  if(isNamespaceLoaded("progressr")) {
    progress <- progressr::progressor(chunks$n)
  } else {
    progress <- function(...) {}
  }

  for(chunk_i in seq_len(chunks$n)) {

    # cached extraction of chunk time series
    chunk_ts_file <- file.path(tmpFolder, "chunks", sprintf("%04d.rds", chunk_i))
    if(!file.exists(chunk_ts_file)) {

      # get chunk data (rows are pixels, cols are layers (=dates))
      chunk_ts <- terra::readValues(ts, row=chunks$row[chunk_i], nrows=chunks$nrows[chunk_i], mat=TRUE)

      saveRDS(chunk_ts, chunk_ts_file)
    } else {
      chunk_ts <- readRDS(chunk_ts_file)
    }

    # cached computation of fused lasso
    chunk_lasso_file <- file.path(tmpFolder, "fused-lasso", sprintf("%04d.rds", chunk_i))
    if(!file.exists(chunk_lasso_file)) {

      # compute lasso for each pixel possibly in parallel
      chunk_lasso <- smartParallel(apply, chunk_ts, 1, pixel_fusedlasso, future.stdout = FALSE)

      # cache result
      saveRDS(chunk_lasso, chunk_lasso_file)
    }

    # handle progression on chunks
    progress()
  }

  # Compute loss threshold
  # ---------------------------------------------------------------- - - -

  sum_runthresh <- unlist(lapply(seq_len(chunks$n), function(chunk_i) {

    chunk_lasso_file <- file.path(tmpFolder, "fused-lasso", sprintf("%04d.rds", chunk_i))
    chunk_lasso <- readRDS(chunk_lasso_file)

    # first order derivative of pixel time series
    # GCO WARNING what if time steps are not constant ?
    deriv1 <- apply(chunk_lasso, 2, diff)
    sum_run <- apply(deriv1, 2, function(l) {
      runner::sum_run(l, k=dayssum, idx=dates[-1], na_rm=TRUE)
    })

    # only take into account losses
    sum_runthresh <- sum_run[sum_run<0]
  }))
  threshold <- stats::quantile(sum_runthresh, perc, na.rm=TRUE)

  # cached detection of breakpoints in fused-lasso regressions
  # ---------------------------------------------------------------- - - -

  for(chunk_i in seq_len(chunks$n)) {

    chunk_ts_file <- file.path(tmpFolder, "chunks", sprintf("%04d.rds", chunk_i))
    chunk_ts <- readRDS(chunk_ts_file)

    chunk_lasso_file <- file.path(tmpFolder, "fused-lasso", sprintf("%04d.rds", chunk_i))
    chunk_lasso <- readRDS(chunk_lasso_file)

    chunk_impact_file <- file.path(tmpFolder, "map-impact", sprintf("%04d.rds", chunk_i))
    if(!file.exists(chunk_impact_file)) {

      # compute impacts
      chunk_impact <- mapImpactsLasso(chunk_ts, dates, chunk_lasso, dayssum, threshold)

      # cache result
      saveRDS(chunk_impact, chunk_impact_file)
    }
  }

  # Build result image from chunks
  # ---------------------------------------------------------------- - - -

  # ensure that output file does not exist (overwrite=TRUE, seems not working)
  if(file.exists(outFile)) {
    file.remove(outFile)
  }

  out <- terra::rast(ts, nlyrs=2)
  names(out) <- c("date", "magnitude")

  tryCatch({
    terra::writeStart(out, outFile)
    for(chunk_i in seq_len(chunks$n)) {

      chunk_data <- readRDS(file.path(tmpFolder, "map-impact", sprintf("%04d.rds", chunk_i)))
      terra::writeValues(out, as.vector(chunk_data), chunks$row[chunk_i], chunks$nrows[chunk_i])

    }
  }, finally = {
    terra::writeStop(out)
  })

  invisible(NULL)
}

