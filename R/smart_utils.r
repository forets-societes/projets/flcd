# build a progressr progressor if available and active
# otherwise return a fake one that does nothing
smartProgressor <- function(..., ..enabled..=TRUE) {
  if(isNamespaceLoaded("progressr") && ..enabled..) {
    progressr::progressor(...)
  } else {
    function(...) {}
  }
}

# alternate version of terra chunking function
# based on maximum pixel count in a chunk instead of cryptic memory consumption
# TODO allow to specify maxSize as an option (ex 500Mo)
smartBlocks <- function(x, maxCells=500000) {
  stopifnot(inherits(x, "SpatRaster"))
  maxCells <- min(maxCells, terra::ncell(x))
  maxCells <- ceiling(maxCells/terra::ncol(x))*terra::ncol(x)
  nrows <- ceiling(maxCells/terra::ncol(x))
  n <- ceiling(terra::nrow(x)/nrows)
  list(
    row = cumsum(c(0, rep(nrows, n-1))) + 1,
    nrows = c(rep(nrows, n-1), terra::nrow(x)-nrows*(n-1)),
    n = n
  )
}

# Depending whether a parallel computing environment is defined (future loaded and some workers) or not
# find a parallel version of ..fun.. in future.apply package and call it with given parameters.
# Otherwise call base version
# ..fun.. can be provided as character or as a symbol
# if ..call.. is FALSE then function won't be called and only be returned
# TODO handle purrr//furrr
smartParallel <- function(..fun.., ..., ..call..=TRUE) {
  ..fun.. <- as.character(substitute(..fun..))
  ..dots.. <- list(...)
  ..f.. <- NULL

  # are we in an active future based parallel environment ?
  if(isNamespaceLoaded("future") && (future::nbrOfWorkers()>1) && requireNamespace("future.apply", quietly=TRUE)) {

    ..f.. <- tryCatch(
      utils::getFromNamespace(paste0("future_", ..fun..), "future.apply"),
      error = function(e) NULL
    )

  }

  if(is.null(..f..)) {

    # remove future specific parameters from parameters
    # otherwise base function will choke on them
    if(!is.null(names(..dots..))) {
      ..dots..[grepl("^future\\.", names(..dots..))] <- NULL
    }

    ..f.. <- tryCatch(
      utils::getFromNamespace(..fun.., "base"),
      error = function(e) NULL
    )
  }

  if(is.null(..f..)) {
    stop("No suitable version found for function `", ..fun.., "` in `base` or `future.apply` packages!")
  }

  if(..call..) {
    do.call(..f.., ..dots.., envir = parent.frame())
  } else {
    ..f..
  }
}

# remove annoying display of current fold in cv.trendfilter
# by replacing calls to `cat` function by a dumb one
patch_genlasso <- function() {
  if(is.null(attr(genlasso::cv.trendfilter, "patched"))) {
    tt <- get("cv.trendfilter", envir=asNamespace("genlasso"), inherits=FALSE)

    body(tt) <- methods::substituteDirect(body(tt), list(cat=function(...) {}))
    attr(tt, "patched") <- TRUE

    unlockBinding("cv.trendfilter", asNamespace("genlasso"))
    assign("cv.trendfilter", tt, envir=asNamespace("genlasso"), inherits = FALSE)
    lockBinding("cv.trendfilter", asNamespace("genlasso"))
    packageStartupMessage("Patched genlasso!")
  }
}

.onLoad <- function(libname, pkgname) {
  patch_genlasso()
}
