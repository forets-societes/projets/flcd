---
output: html_document
---
# FLCD

## Description of the FLCD method

The fused-lasso developed by Tibshirani et al. (2005)[^1], enabling the
modeling of abrupt changes in time series, has been applied to forest
monitoring as a new method called Fused-Lasso Change Detection (FLCD).

[^1]: Tibshirani, Robert, Michael Saunders, Saharon Rosset, Ji Zhu, and
    Keith Knight. 2005. "Sparsity and Smoothness Via the Fused Lasso."
    Journal of the Royal Statistical Society Series B: Statistical
    Methodology 67 (1): 91--108.
    <https://doi.org/10.1111/j.1467-9868.2005.00490.x>.

To produce maps of small-scale forest disturbance from the Sentinel-1 SAR time series, we developed a method we call Fused-Lasso Change Detection (FLCD). First, fused-lasso regressions are computed on Sentinel-1 time series. Secondly, iterative local differences between successive values and corresponding running sums are applied to highlight signal decreases. Finally, a post-classification step based on the disturbance dates is applied to reduce overdetections.

For further details on the FLCD method, see Mercier A., Betbeder J., Mortier F. ...

If using the method, please cite : Mercier A., Betbeder J., Mortier F. ...

Funding: This research was funded through the PROFEAAC project funded by the French Development Agency (AFD) (AGREEMENT AFD No. CZZ 2123.01 V)

## Installation

You can install the development version of FLCD like so:

``` r
remotes::install_gitlab("forets-societes/projets/flcd", host="gitlab.cirad.fr")
```

## Technical details

To reduce memory load during processing of big raster time series, the
stack is split by pack of consecutive rows in chunks. Each chunk is then
sequentially processed. Chunk result are cached until the end of the
FLCD computation so if anything goes wrong or the process needs to be
stopped at some time, it will restart where it was. `maxPixels`
parameter is there to control chunk size. You can reduce its value if
you are hitting memory limits or need finer control of progress.

To reduce computing time, parallel processing is leveraged thanks to
`future` and it's sibling `future.apply` package. During processing of a
chunk, pixels will be dispatched among workers for the fused lasso step.

Progress reporting can be done using `progressr` package through the
`with_progress` function (see example code below).

Note: Parallelization and progress reporting are not mandatory and
everything will run without `future` and `progressr` packages. It only
will take more time to compute with no progress reporting.

## Example

This is a basic example to produce forest disturbance maps from the FLCD method.

uav2019vv is a SpatRaster object consisting of a Sentinel-1 time series from 2019-01-01 to 2019-12-31 on a subsite in a FSC logging area in the Republic of Congo.

``` r
library(FLCD)
library(terra)
library(future)
library(progressr)

ts <- rast(system.file("ex/uav2019vv.tif", package="FLCD"))
dates <- as.Date(names(ts), "%Y-%m-%d")

# parallel processing is not mandatory but it will take more time to compute
plan(multisession)
with_progress(computeFLCD(ts, dates, "uav2019vv-impacts.tif", maxPixels=1000))
plan(sequential)

removeIsolatedImpacts("uav2019vv-impacts.tif", "uav2019vv-cleaned-impacts.tif")
```
### Raw impacts (uav2019vv-impacts.tif)
Dates are number of days since 1970-01-01.

![Raw impacts detection date and magnitude](man/figures/impacts.png)

### Postprocessed impacts (uav2019vv-cleaned-impacts.tif)

![Postprocessed impacts detection date and magnitude](man/figures/impacts-cleaned.png)
